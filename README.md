Relying on a rate constraint to reduce ME complexity
====

This is a [VTM](https://vcgit.hhi.fraunhofer.de/jvet/VVCSoftware_VTM) fork dedicated to implementing the algorithm presented in the paper *Relying on a rate constraint to reduce Motion Estimation complexity*, which was submitted to ICASSP 2021.

We've developed on top of [VTM-6.2](https://vcgit.hhi.fraunhofer.de/jvet/VVCSoftware_VTM/-/tags/VTM-6.2) as a starting point, and modifications to the original codebase were kept to a minimum in order to reduce diffs.
This also means that the same [BSD license](./COPYING) applies to this repository.

Also note that the configurations we've used ([LDP](cgf/encoder_lowdelay_P_vtm.cfg) and [RA](cfg/encoder_randomaccess_vtm.cfg)) have `InternalBitDepth` set to 8 over its VTM-6.2 default of 10.
This is done because the setting unexpectedly affects the external representation of encoded video bitstreams.

Auxiliary scripts used to run experiments, parse logs, compile results and plot figures related to the paper can be found inside the [ecl](ecl/) folder.

To build VTM using CMake on Linux:

```bash
mkdir build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Release
make
```

For other platforms, consult the original [build instructions](https://vcgit.hhi.fraunhofer.de/jvet/VVCSoftware_VTM/-/blob/master/README.md#build-instructions).
