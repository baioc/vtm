#ifndef ECL_ACCUMULATOR_H
#define ECL_ACCUMULATOR_H

#include <cstddef> // size_t
#include <algorithm> // min, max
#include <cmath> // sqrt


namespace ecl {

/**
 * @brief Helper class providing an accumulator that keeps track of min, max
 * and increment count, thus being able to provide overall mean and std values.
 *
 * @tparam T - internally used algebraic type.
 */
template <typename T>
class Accumulator { // this class is probably misnamed
 public:
	Accumulator() = default;
	Accumulator(T init): sum_(init), squares_(init * init), count_{1} {}

	inline void increment(T value)
	{
		sum_ += value;
		count_ += 1;
		const bool initialized = count_ > 0;
		min_ = initialized ? std::min(min_, value) : value;
		max_ = initialized ? std::max(max_, value) : value;
		squares_ += value * value;
	}

	inline T sum() const noexcept { return sum_; }
	inline T min() const noexcept { return min_; }
	inline T max() const noexcept { return max_; }
	inline std::size_t count() const noexcept { return count_; }
	inline T mean() const noexcept { return sum_ / count_; }
	inline T std() const noexcept {
		const auto average = mean();
		const auto variance = (squares_ / count_) - (average * average);
		return std::sqrt(variance);
	}

 private:
	T sum_ = 0;
	std::size_t count_{0};
	T min_ = sum_;
	T max_ = sum_;
	T squares_ = 0;
};

} // namespace ecl

#endif // ECL_ACCUMULATOR_H
