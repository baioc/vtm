#include "monitor.h"

#include <string>
#include <ostream>
#include <unordered_map>
#include <vector>
#include <functional> // function


namespace ecl {

Monitor* monitor_instance_ = nullptr;

Monitor& Monitor::get() noexcept
{
	if (!monitor_instance_)
		monitor_instance_ = new Monitor();
	return *monitor_instance_;
}

void Monitor::update(const std::string& label, const std::string& value)
{
	const auto it = updaters_.find(label);
	if (it != updaters_.end())
		it->second(data_[label], value);
	else
		data_[label].push_back(value);
}

void Monitor::print(std::ostream& output) const
{
	for (const auto& variable: data_) {
		output << variable.first << '\n';

		for (const auto& entry: variable.second)
			output << entry << '\n';

		output << '\n';
	}
}

std::ostream& operator<<(std::ostream& output, const Monitor& monitor)
{
	monitor.print(output);
	return output;
}

const std::vector<std::string>& Monitor::retrieve(
	const std::string& label
) const noexcept(false)
{
	return data_.at(label);
}

void Monitor::track(
	const std::string& label,
	std::function<void (std::vector<std::string>&, const std::string&)> proc)
{
	updaters_[label] = proc;
}

} // namespace ecl
