#ifndef ECL_MONITOR_H
#define ECL_MONITOR_H

#include <string> // to_string
#include <ostream>
#include <unordered_map>
#include <vector>
#include <functional> // function


namespace ecl {

/**
 * @brief Singleton class that keeps track of multiple variables,
 * each uniquely identified by some label.
 */
class Monitor {
 public:
	/**
	 * @brief Gets a handle to the singleton.
	 * @return Monitor& - instace reference.
	 */
	static Monitor& get() noexcept;

	/**
	 * @brief Adds a new observation to a certain variable.
	 * @param label - variable identifier.
	 * @param value - new value.
	 */
	void update(const std::string& label, const std::string& value);

	/**
	 * @brief Adds a new observation to a certain variable.
	 * @tparam T - requires an implementation of to_string(T).
	 * @param label - variable identifier.
	 * @param value - new value.
	 */
	template <typename T>
	inline void update(const std::string& label, const T& value)
	{
		using std::to_string;
		update(label, to_string(value));
	}

	/**
	 * @brief Prints every monitored variable to some output stream.
	 * @param output - output stream to pipe data into.
	 */
	void print(std::ostream& output) const;

	/**
	 * @brief Retrieves some variable's history. If no such element exists, an
	 * exception of type std::out_of_range is thrown.
	 * @param label - variable identifier.
	 * @return std::vector<std::string> - ordered iterable container with the
	 * monitored variable's history.
	 */
	const std::vector<std::string>& retrieve(
		const std::string& label
	) const noexcept(false);

	/**
	 * @brief Registers a new variable and changes its default update behaviour.
	 * @param label - variable identifier.
	 * @param proc - procedure that is called at each update with a reference
	 * to the current history and each new observation.
	 */
	void track(
		const std::string& label,
		std::function<void (std::vector<std::string>&, const std::string&)> proc
	);

 private:
	Monitor() noexcept = default;
	std::unordered_map<std::string,std::vector<std::string>> data_;
	std::unordered_map<
		std::string,
		std::function<void (std::vector<std::string>&, const std::string&)>
	> updaters_;
};

/** @brief Provides syntax sugar to print a monitor. */
std::ostream& operator<<(std::ostream& output, const Monitor& monitor);

extern Monitor* monitor_instance_;

} // namespace ecl

#endif // ECL_MONITOR_H
