/* The copyright in this software is being made available under the BSD
 * License, included below. This software may be subject to other third party
 * and contributor rights, including patent rights, and no such rights are
 * granted under this license.
 *
 * Copyright (c) 2010-2019, ITU/ISO/IEC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  * Neither the name of the ITU/ISO/IEC nor the names of its contributors may
 *    be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

/** \file     encmain.cpp
    \brief    Encoder application main
*/

#include <time.h>
#include <iostream>
#include <chrono>
#include <ctime>
#include <cstddef> // size_t
#include <string> // to_string
#include <sstream> // isstream
#include <cstdlib> // atof, atoi ...

#include "EncApp.h"
#include "Utilities/program_options_lite.h"
#include "CommonLib/monitor.h" // Monitor
#include "CommonLib/accumulator.h" // Accumulator

//! \ingroup EncoderApp
//! \{

static const uint32_t settingNameWidth = 66;
static const uint32_t settingHelpWidth = 84;
static const uint32_t settingValueWidth = 3;
// --------------------------------------------------------------------------------------------------------------------- //

//macro value printing function

#define PRINT_CONSTANT(NAME, NAME_WIDTH, VALUE_WIDTH) std::cout << std::setw(NAME_WIDTH) << #NAME << " = " << std::setw(VALUE_WIDTH) << NAME << std::endl;

static void printMacroSettings()
{
  if( g_verbosity >= DETAILS )
  {
    std::cout << "Non-environment-variable-controlled macros set as follows: \n" << std::endl;

    //------------------------------------------------

    //setting macros

    PRINT_CONSTANT( RExt__DECODER_DEBUG_BIT_STATISTICS,                         settingNameWidth, settingValueWidth );
    PRINT_CONSTANT( RExt__HIGH_BIT_DEPTH_SUPPORT,                               settingNameWidth, settingValueWidth );
    PRINT_CONSTANT( RExt__HIGH_PRECISION_FORWARD_TRANSFORM,                     settingNameWidth, settingValueWidth );
    PRINT_CONSTANT( ME_ENABLE_ROUNDING_OF_MVS,                                  settingNameWidth, settingValueWidth );

    //------------------------------------------------

    std::cout << std::endl;
  }
}

// ====================================================================================================================
// Main function
// ====================================================================================================================

// helper functions to print as JSON
template <typename T>
std::string to_json(const T& x) { return std::to_string(x); }

template <typename T>
std::string to_json(const ecl::Accumulator<T>& acc)
{
  const std::string s = "{";
  return s +
    "\"count\": " + to_json(acc.count()) + ", " +
    "\"min\": " + to_json(acc.min()) + ", " +
    "\"max\": " + to_json(acc.max()) + ", " +
    "\"mean\": " + to_json(acc.mean()) + ", " +
    "\"std\": " + to_json(acc.std()) + "}";
}

template <>
std::string to_json(const std::string& str) { return '"' + str + '"'; }

template <typename K, typename V>
std::string to_json(const std::map<K,V>& map)
{
  std::string acc = "{";
  bool delim = false;
  for (const auto& entry : map) {
    const auto elem = to_json(entry.first) + ": " + to_json(entry.second);
    acc += delim ? ", " + elem : elem;
    delim = true;
  }
  return acc + '}';
}

template <> // only works with this specialization for some reason
std::string to_json(const std::map<uint32_t,float>& map)
{
  std::string acc = "{";
  bool delim = false;
  for (const auto& entry : map) {
    const auto elem = '"' + to_json(entry.first) + "\": " + to_json(entry.second);
    acc += delim ? ", " + elem : elem;
    delim = true;
  }
  return acc + '}';
}


int main(int argc, char* argv[])
{
  // print information
  fprintf( stdout, "\n" );
  fprintf( stdout, "VVCSoftware: VTM Encoder Version %s ", VTM_VERSION );
  fprintf( stdout, NVM_ONOS );
  fprintf( stdout, NVM_COMPILEDBY );
  fprintf( stdout, NVM_BITS );
#if ENABLE_SIMD_OPT
  std::string SIMD;
  df::program_options_lite::Options opts;
  opts.addOptions()
    ( "SIMD", SIMD, string( "" ), "" )
    ( "c", df::program_options_lite::parseConfigFile, "" );
  df::program_options_lite::SilentReporter err;
  df::program_options_lite::scanArgv( opts, argc, ( const char** ) argv, err );
  fprintf( stdout, "[SIMD=%s] ", read_x86_extension( SIMD ) );
#endif
#if ENABLE_TRACING
  fprintf( stdout, "[ENABLE_TRACING] " );
#endif
#if ENABLE_SPLIT_PARALLELISM
  fprintf( stdout, "[SPLIT_PARALLEL (%d jobs)]", PARL_SPLIT_MAX_NUM_JOBS );
#endif
#if ENABLE_WPP_PARALLELISM
  fprintf( stdout, "[WPP_PARALLEL]" );
#endif
#if ENABLE_WPP_PARALLELISM || ENABLE_SPLIT_PARALLELISM
  const char* waitPolicy = getenv( "OMP_WAIT_POLICY" );
  const char* maxThLim   = getenv( "OMP_THREAD_LIMIT" );
  fprintf( stdout, waitPolicy ? "[OMP: WAIT_POLICY=%s," : "[OMP: WAIT_POLICY=,", waitPolicy );
  fprintf( stdout, maxThLim   ? "THREAD_LIMIT=%s" : "THREAD_LIMIT=", maxThLim );
  fprintf( stdout, "]" );
#endif
  fprintf( stdout, "\n" );

  EncApp* pcEncApp = new EncApp;
  // create application encoder class
  pcEncApp->create();

  // parse configuration
  try
  {
    if(!pcEncApp->parseCfg( argc, argv ))
    {
      pcEncApp->destroy();
      return 1;
    }
  }
  catch (df::program_options_lite::ParseFailure &e)
  {
    std::cerr << "Error parsing option \""<< e.arg <<"\" with argument \""<< e.val <<"\"." << std::endl;
    return 1;
  }

  // setup encoder data extraction tools
  using acc = ecl::Accumulator<long double>;
  auto& monitor = ecl::Monitor::get();

  acc candidate_area;
  monitor.track("Candidate Area", [&candidate_area](std::vector<std::string>& ignored, const std::string& area){
    candidate_area.increment(std::atof(area.c_str()));
  });

  std::map<std::string, std::map<std::string,acc>> per_block_size;
  const std::string all = "*";
  const std::string features = "Block Size,Predictor Length,Motion Vector Length,Distortion,Rate";
  // @NOTE: I wouldn't trust predictor/MV lengths since we didn't take precision into account when calculating them
  std::map<uint32_t,std::size_t> rates_count;
  std::size_t rates_total = 0;
  monitor.track(features, [&per_block_size, &all, &rates_count, &rates_total]
                          (std::vector<std::string>& ignored, const std::string& observation)
    {
      std::istringstream stream(observation);
      std::string token;

      std::getline(stream, token, ',');
      const std::string size = token;

      std::getline(stream, token, ',');
      const float pred = std::atof(token.c_str());

      std::getline(stream, token, ',');
      const float mv = std::atof(token.c_str());

      std::getline(stream, token, ',');
      const Distortion sad = static_cast<Distortion>(std::atoll(token.c_str()));

      std::getline(stream, token, ',');
      const uint32_t rate = static_cast<uint32_t>(std::atoi(token.c_str()));

      per_block_size[all]["Predictor Length"].increment(pred);
      per_block_size[size]["Predictor Length"].increment(pred);
      per_block_size[all]["Motion Vector Length"].increment(mv);
      per_block_size[size]["Motion Vector Length"].increment(mv);
      per_block_size[all]["Distortion"].increment(sad);
      per_block_size[size]["Distortion"].increment(sad);
      per_block_size[all]["Rate"].increment(rate);
      per_block_size[size]["Rate"].increment(rate);

      rates_count[rate] += 1;
      rates_total += 1;
  });

  // get video filename
  const std::string path = pcEncApp->m_bitstreamFileName;
  const auto begin = path.find_last_of('/') + 1;
  const auto end = path.find_last_of('.');
  const auto video = path.substr(begin, end - begin);

#if PRINT_MACRO_VALUES
  printMacroSettings();
#endif

  // starting time
  auto startTime  = std::chrono::steady_clock::now();
  std::time_t startTime2 = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
  fprintf(stdout, " started @ %s", std::ctime(&startTime2) );
  clock_t startClock = clock();

  // call encoding function
#ifndef _DEBUG
  try
  {
#endif
    pcEncApp->encode();
#ifndef _DEBUG
  }
  catch( Exception &e )
  {
    std::cerr << e.what() << std::endl;
    return EXIT_FAILURE;
  }
  catch (const std::bad_alloc &e)
  {
    std::cout << "Memory allocation failed: " << e.what() << std::endl;
    return EXIT_FAILURE;
  }
#endif
  // ending time
  clock_t endClock = clock();
  auto endTime = std::chrono::steady_clock::now();
  std::time_t endTime2 = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
#if JVET_O0756_CALCULATE_HDRMETRICS
  auto metricTime     = pcEncApp->getMetricTime();
  auto totalTime      = std::chrono::duration_cast<std::chrono::milliseconds>( endTime - startTime ).count();
  auto encTime        = std::chrono::duration_cast<std::chrono::milliseconds>( endTime - startTime - metricTime ).count();
  auto metricTimeuser = std::chrono::duration_cast<std::chrono::milliseconds>( metricTime ).count();
#else
  auto encTime = std::chrono::duration_cast<std::chrono::milliseconds>( endTime - startTime).count();
#endif
  // destroy application encoder class
  pcEncApp->destroy();

  delete pcEncApp;

  printf( "\n finished @ %s", std::ctime(&endTime2) );

#if JVET_O0756_CALCULATE_HDRMETRICS
  printf(" Encoding Time (Total Time): %12.3f ( %12.3f ) sec. [user] %12.3f ( %12.3f ) sec. [elapsed]\n",
         ((endClock - startClock) * 1.0 / CLOCKS_PER_SEC) - (metricTimeuser/1000.0),
         (endClock - startClock) * 1.0 / CLOCKS_PER_SEC,
         encTime / 1000.0,
         totalTime / 1000.0);
#else
  printf(" Total Time: %12.3f sec. [user] %12.3f sec. [elapsed]\n",
         (endClock - startClock) * 1.0 / CLOCKS_PER_SEC,
         encTime / 1000.0);
#endif

  // print monitored data summary (used as complexity measure)
  std::cout << "\nTotal Candidate Area: " << candidate_area.sum() << '\n';
  std::cout << "\nNumber of Candidates: " << candidate_area.count() << '\n';

  // make JSON statistics report
  std::map<uint32_t,float> rates;
  for (const auto& kv : rates_count)
    rates[kv.first] = kv.second / static_cast<float>(rates_total);

  const auto overall = per_block_size[all];
  per_block_size.erase(all);

  std::ofstream stats(video + ".json");
  stats << '{'
        << "\"Overall\":" << to_json(overall) << ", "
        << "\"Grouped by Block Size\": " << to_json(per_block_size) << ", "
        << "\"Rates\": " << to_json(rates) << '}';

  return 0;
}

//! \}
