# Eliminação de Candidatos baseada em Taxa


### [bitrate.py](scripts/bitrate.py)

Plota o gráfico da superfície de taxa para um bloco 128x128.

#### Uso

```shell
$ python bitrate.py [thresholds...]
```

Todos os argumentos adicionais são lidos como thresholds, cujo delineado aparece na figura plotada.

##### Exemplo

```shell
$ python scripts/bitrate.py 4 10 20
```


### [motionmap.py](scripts/motionmap.py)

Executa o Decoder e plota um heatmap dos MVs.

#### Uso

```shell
$ ./script.py vtm/decoder_app sequence_qQP.vvc ...
```

Todos os argumentos a partir do segundo são lidos como arquivos de bitstream a serem decodificados.

Para configurar o script, editar o seguinte trecho:
```python
# constants
XLIMS = (-64, 63)
YLIMS = (-63, 64)
STEP = 16
```

##### Exemplo

```bash
$ scripts/motionmap.py ~/VTM/bin/DecoderAppStatic str/original/*.vvc
```


### [run_vtm.sh](scripts/run_vtm.sh)

Executa o Encoder para um vídeo, com um certo QP e para um dado número de frames.

#### Uso

```shell
$ ./run_vtm.sh path/to/vtm/folder frames_to_decode path/to/raw.yuv qp
```

Os arquivos resultantes são automaticamente colocados nas pastas `dec`, `log` e `str`.

O arquivo de estatísticas `.json `é gerado somente ao fim da codificação e é deixado no diretório atual.

O arquivo de configuração geral do Encoder deve ser editado manualmente, no seguinte trecho do script:

```bash
# choose one config to uncomment
config='encoder_lowdelay_P_vtm.cfg'
# config='encoder_randomaccess_vtm.cfg'
```

##### Exemplo

```shell
$ scripts/run_vtm.sh ~/VTM 300 ~/Videos/RAW/RaceHorses.yuv 22
```


### [process_logs.py](scripts/process_logs.py)

Transforma os logs do Encoder em uma tabela.

#### Uso

```shell
$ ./process_logs.py [-h] [-s sep=","] [-t technique] encoder.log ... >> tests.csv
```

Se o parâmetro `-h` for dado, a primeira linha na saída será o header da tabela.

O parâmetro `-s sep` define o separador utilizado (por padrão, uma vírgula).

O parâmetro `-t technique` adiciona uma coluna na tabela para identificar o algoritmo utilizado.

##### Exemplo

```shell
$ python scripts/process_logs.py -t "Original" -h log/original/*.log > pruning_tests.csv # cria o arquivo com dados originais, inserindo o header
$ python scripts/process_logs.py -t "Prune R > 4" log/pruned/*.log >> pruning_tests.csv # adiciona dados da eliminacao
```


### [generate_results.py](scripts/generate_results.py)

Gera uma tabela comparando um algoritmo de eliminação com o original.

Inclui BD-Rate e taxa de complexidade média para os 4 QPs de uma mesma sequência.

#### Uso

```shelll
$ ./generate_results.py tests.csv path/to/vqtool
```

Os resultados são colocados em um arquivo chamado `results.csv` no diretório atual.

A tabela de entrada deve ter sido gerada com o script [process_logs.py](scripts/process_logs.py) e deve conter os dados originais ("Original") para comparação.

`vqtool` é o programa do Ismael para cálculo de BD-Rate.

##### Exemplo

```shell
$ python scripts/generate_results.py pruning_tests.csv ~/ECL/video_quality_analysis/vqtool/dist/vqtool
```


### [cg_plot](cg_plot/)

Geração de plots de redução de complexidade por BD-Rate:
- [make_plottable_data.py](cg_plot/make_plottable_data.py)
- [plot.py](cg_plot/plot.py)

#### Uso

```shell
$ python make_plottable_data.py results.csv data/
$ python plot.py
```

O primeiro script gera os dados no formato esperado pelo segundo, a tabela de entrada deve ter sido gerada com o script [generate_results.py](scripts/generate_results.py).

O segundo é o script do Luiz, que lê os dados da pasta `data/` e gera os plots na pasta `plot/`.
Para configurar os plots, editar o script diretamente.

##### Exemplo

```shell
$ python scripts/cg_plot/make_plottable_data.py ../../results/pruning_results.csv ./data/
$ cd scripts/cg_plot
$ python plot.py
```
