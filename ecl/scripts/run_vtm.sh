#!/bin/bash

# usage: ./script.sh path/to/vtm frames path/to/raw.yuv qp
vtm_dir=$1
frame_number=$2
yuv=$3
qp=$4

# choose one config to uncomment
config='encoder_lowdelay_P_vtm.cfg'
# config='encoder_randomaccess_vtm.cfg'

cd=$(pwd)

video=$(basename "${yuv%.*}")

command=""$vtm_dir"/bin/EncoderAppStatic \
-c "$vtm_dir"/cfg/"$config" \
-c "$vtm_dir"/cfg/per-sequence/"$video".cfg \
-q "$qp" \
-i "$yuv" \
-o "$cd"/dec/"$video"_q"$qp"_vtm.yuv \
-b "$cd"/str/"$video"_q"$qp".vvc \
-f "$frame_number" \
--MSEBasedSequencePSNR=1 \
> "$cd"/log/"$video"_q"$qp"_vtm.log"

eval $command
