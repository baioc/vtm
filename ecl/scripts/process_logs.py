#! /usr/bin/env python
from sys import argv

# usage: ./script.py [-h] [-s sep=","] [-t technique] Sequence_qQP_vtm.log... >> tab.csv
put_header = '-h' in argv
has_sep = '-s' in argv
has_technique = '-t' in argv
input_files = argv[1 + put_header + has_sep*2 + has_technique*2: ]
sep = argv[argv.index('-s') + 1] if has_sep else ','
technique = argv[argv.index('-t') + 1] if has_technique else None

if put_header:
    fields = ['Sequence', 'QP', 'Time (sec.)', 'Bitrate', 'PSNR', 'Area (px)', 'Candidates']
    fields = ['Technique'] + fields if has_technique else fields
    print(sep.join(fields))

for filepath in input_files:
    sequence, qp, time, bitrate, psnr, area, candidates = [''] * 7

    with open(filepath) as report:
        for line in report:
            #          Total Frames |  Bitrate    Y-PSNR  ...
            # Average:      300     a  116.4464   29.5220 ...
            if 'Average:' in line:
                _, _, _, bitrate, psnr, *_ = line.strip().split()
            # Total Time: 3812.796 sec. [user] 3819.455 sec. [elapsed]
            elif 'Total Time:' in line:
                time = line.strip().split()[2]
            # Total Candidate Area: 4.46638902496e+11
            elif 'Total Candidate Area:' in line:
                area = line.strip().split()[3]
                area = str(int(round(float(area))))
            # Number of Candidates: 1657369242
            elif 'Number of Candidates:' in line:
                candidates = line.strip().split()[3]

    # expected filename: Sequence_qQP_vtm.log
    filename = filepath.split('/')[-1].split('.')[0]
    qp_pos = filename.find('_q') + 2
    sequence = filename[:qp_pos - 2]
    qp = filename[qp_pos:qp_pos + 2]

    fields = [sequence, qp, time, bitrate, psnr, area, candidates]
    fields = [technique] + fields if has_technique else fields
    print(sep.join(fields))
