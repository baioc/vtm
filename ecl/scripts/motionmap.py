#! /usr/bin/env python
from sys import argv, stderr
import subprocess
from os import path
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.colors import Normalize
import numpy as np

# usage: ./script.py vtm/decoder_app Sequence_qQP.vvc...
vtm_decoder = argv[1]
vvc_encoded = argv[2:]

# constants
XLIMS = (-64, 63)
YLIMS = (-63, 64)
STEP = 16

# decode each bitstream and record its MV data
xs, ys = [], []
for bitstream in vvc_encoded:
    # check if decoder already ran for this file and run it otherwise
    sequence = bitstream.split('/')[-1].split('.')[0]
    decoder_data = sequence + '.csv'
    if not path.isfile(decoder_data):
        try:
            print("Decoding %s ..." % sequence)
            cmd = [vtm_decoder, '-b', bitstream]
            subprocess.check_output(cmd)
        except subprocess.CalledProcessError as error:
            print(sequence, "error:", error.returncode, error.output, file=stderr)
            exit(error.returncode)

    # read table generated with decoder data
    print("Checking decoder data at %s ..." % decoder_data)
    MV = {0: {'x': 'MVD0 Hor', 'y': 'MVD0 Ver'}, 1: {'x': 'MVD1 Hor', 'y': 'MVD1 Ver'}}
    cols = ['Prediction Mode', 'L0', 'L1', MV[0]['x'], MV[0]['y'], MV[1]['x'], MV[1]['y'], 'imvShift']
    df = pd.read_csv(decoder_data, usecols=cols)

    # filter for Inter prediction only
    df = df[df['Prediction Mode'].str.contains('INTER')]

    # record each MVD
    def record_mvd(x, y, shift):
        xs.append(x >> shift)
        ys.append(y >> shift)
    for _, row in df.iterrows():
        if row['L0'] == 1:
            record_mvd(row[MV[0]['x']], row[MV[0]['y']], row['imvShift'])
        if row['L1'] == 1:
            record_mvd(row[MV[1]['x']], row[MV[1]['y']], row['imvShift'])

# setup plot matrix
x_min = min(xs)
x_max = max(xs)
y_min = min(ys)
y_max = max(ys)
n = len(xs)
zs = np.zeros((y_max - y_min + 1, x_max - x_min + 1))
for x, y in zip(xs, ys):
    zs[y - y_min][x - x_min] += 1 / n

# found this to plot a logarithmic color range
class PiecewiseNorm(Normalize):
    def __init__(self, levels, clip=False):
        self._levels = np.sort(levels)
        self._normed = np.linspace(0, 1, len(levels))
        Normalize.__init__(self, None, None, clip)
    def __call__(self, value, clip=None):
        return np.ma.masked_array(np.interp(value, self._levels, self._normed))

# crop array to the more interesting region
zs = zs[YLIMS[0] - y_min : YLIMS[1] - y_min + 1, XLIMS[0] - x_min : XLIMS[1] - x_min + 1]
extent = [XLIMS[0], XLIMS[1], YLIMS[0], YLIMS[1]] #[x_min, x_max, y_min, y_max]

# plot MV heatmap
levels = [p / n for p in np.geomspace(1, n) if p / n <= np.amax(zs)]
levels[0] = 0
img = plt.contourf(zs, levels, cmap='hot', norm=PiecewiseNorm(levels), extent=extent)
plt.colorbar(img, format='%e', label='%')
plt.xlabel("X")
plt.xticks([*range(XLIMS[0], XLIMS[1] + 1, STEP)] + [XLIMS[1]])
plt.ylabel("Y", rotation='horizontal', verticalalignment='center')
plt.yticks([YLIMS[0]] + [*range(YLIMS[0] - 1 + STEP, 0, STEP)] + [*range(0, YLIMS[1] + 1, STEP)])
plt.show()
