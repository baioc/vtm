#! /usr/bin/env python
from sys import argv, stderr
import pandas as pd
import subprocess
from os import remove

# usage: ./script.py tests.csv vqtool.bin
input_file = argv[1]
vqtool_bin = argv[2]

tests = pd.read_csv(input_file)
result_list = []

for sequence in tests.Sequence.unique():
    # extract sequence-specific test data
    seq = tests[tests.Sequence == sequence]
    original = seq[seq.Technique == 'Original']
    test = seq[seq.Technique != 'Original']
    assert len(original) == len(test)

    # compute other rates
    CAN, AREA, TIME = 'Candidates', 'Area (px)', 'Time (sec.)'
    crate = (test[CAN].values / original[CAN].values).mean()
    arate = (test[AREA].values / original[AREA].values).mean()
    trate = (test[TIME].values / original[TIME].values).mean()

    # print Bitrate and PSNR to tables
    rd_ori = sequence + '_ori' + '.tsv'
    rd_test = sequence + '_test' + '.tsv'
    original.sort_values('QP')[['Bitrate', 'PSNR']].to_csv(rd_ori, index=False, sep='\t')
    test.sort_values('QP')[['Bitrate', 'PSNR']].to_csv(rd_test, index=False, sep='\t')

    try:
        # check overlay
        overlay_cmd = [vqtool_bin, '--metric', 'overlay', rd_ori, rd_test]
        overlay = float(subprocess.check_output(overlay_cmd).decode().strip())
        if (overlay < 0.75):
            print(sequence, "overlay:", overlay, file=stderr)

        # compute bdrate and store metrics for this sequence
        bdrate_cmd = [vqtool_bin, '--metric', 'bdrate', rd_ori, rd_test]
        bdrate = subprocess.check_output(bdrate_cmd)
        bdrate = float(bdrate.decode().strip()) / 100
        result_list.append({
            'Sequence': sequence,
            'BD Rate': bdrate,
            'Average Candidate Number Rate': crate,
            'Average Candidate Area Rate': arate,
            'Average Time Rate': trate,
        })

        # remove temporary table files
        remove(rd_ori)
        remove(rd_test)

    except subprocess.CalledProcessError as error:
        print(sequence, "error: ", error.returncode, error.output, file=stderr)

pd.DataFrame(result_list).to_csv('results.csv', index=False)
