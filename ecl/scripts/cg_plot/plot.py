import os
import shutil

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


#[TODO] select samples
samples = [
    'BasketballPass',
    'BlowingBubbles',
    'BQSquare',
    'RaceHorses',
    'BasketballDrill',
    'PartyScene',
    'BQMall',
    'RaceHorsesC',
    'BasketballDrillText',
    'SlideEditing',
    'SlideShow',
    # 'ChinaSpeed',
    'FourPeople',
    'Johnny',
    'KristenAndSara',
    'BasketballDrive',
    'BQTerrace',
    'Cactus',
    # 'ParkScene',
    # 'Kimono',
    # 'PeopleOnStreet',
    # 'Traffic'
]

# sample : [title, line, [marker, facecolor, edgecolor , color]
sample_style = {
    'Traffic' : ['Traffic (2560x1600)', 						['|', '#332288','#332288'],			'#332288'] ,
    'PeopleOnStreet' : ['PeopleOnStreet (2560x1600)', 			['x', '#332288','#332288'],			'#332288'] ,
    'Kimono' : ['Kimono (1920x1080)', 							[(6,2,0), '#117733','#117733'],		'#117733'] ,

    'ParkScene' : ['ParkScene (1920x1080)', 					['s', 'None','#117733'],			'#117733'] ,
    'Cactus' : ['Cactus (1920x1080)', 							['s', '#117733','#117733'],			'#117733'] ,
    'BQTerrace' : ['BQTerrace (1920x1080)',						['o', 'None','#117733'],			'#117733'] ,

    'BasketballDrive' : ['BasketballDrive (1920x1080)', 		['o', '#117733','#117733'],			'#117733'] ,
    'RaceHorsesC' : ['RaceHorsesC (832x480)', 					['^', 'None','#DDCC77'],			'#DDCC77'] ,
    'BQMall' : ['BQMall (832x480)', 							['^', '#DDCC77','#DDCC77'],			'#DDCC77'] ,
    'PartyScene' : ['PartyScene (832x480)', 					['v', 'None','#DDCC77'],			'#DDCC77'] ,

    'BasketballDrill' : ['BasketballDrill (832x480)', 			['v', '#DDCC77','#DDCC77'],			'#DDCC77'] ,
    'RaceHorses' : ['RaceHorses (416x240)', 					['D', 'None','#CC6677'],			'#CC6677'] ,
    'BQSquare' : ['BQSquare (416x240)', 						['D', '#CC6677','#CC6677'],			'#CC6677'] ,

    'BlowingBubbles' : ['BlowingBubbles (416x240)', 			['p', 'None','#CC6677'],			'#CC6677'] ,
    'BasketballPass' : ['BasketballPass (416x240)', 			['p', '#CC6677','#CC6677'],			'#CC6677'] ,
    'FourPeople' : ['FourPeople (1280x720)', 					['|', '#AA4499','#AA4499'],			'#AA4499'] ,

    'Johnny' : ['Johnny (1280x720)', 							['x', '#AA4499','#AA4499'],			'#AA4499'] ,
    'KristenAndSara' : ['KristenAndSara (1280x720)', 			[(6,2,0), '#AA4499','#AA4499'],		'#AA4499'] ,
    'BasketballDrillText' : ['BasketballDrillText (832x480)', 	['s', 'None','#44AA99'],			'#44AA99'] ,

    'ChinaSpeed' : ['ChinaSpeed (1024x768)', 					['s', '#44AA99','#44AA99'],			'#44AA99'] ,
    'SlideEditing' : ['SlideEditing (1280x720)', 				['o', 'None','#44AA99'],			'#44AA99'] ,
    'SlideShow' : ['SlideShow (1280x720)', 						['o', '#44AA99','#44AA99'],			'#44AA99'] ,
    'avg' : ['Average', 										['*', '#000000','#000000'],			'#000000']
}


def extract_2D_data(in_file, sep=','):
    data_file = pd.read_csv(in_file, sep=sep)
    key = data_file.columns[0]
    group_labels = [label for label in data_file.columns[1:]]
    x_ticks_labels = list(data_file[key])
    data = []
    for column in group_labels:
        y_data = (list(data_file[column]))
        data.append(y_data)
    return x_ticks_labels, group_labels, data


def plot_all_lines(title, info_map, labels, x_ticks, y_ticks, out_path, size=100, target_format='png'):
    fig, ax = plt.subplots(figsize=(8, 4))

    for key, info in info_map.items():
        style = info[1]
        title = style[0]
        marker = style[1][0]
        marker_face = style[1][1]
        marker_edge = style[1][2]
        color = style[2]

        data = info[0]
        x_values = data[0]
        y_values = data[1]

        ax.plot(x_values, y_values, color=color, marker=marker, markerfacecolor=marker_face, markeredgecolor=marker_edge, linewidth=1.2, label = title, ms=10, zorder=1)

    # ax.set_title(title)
    ax.set_xlabel(labels[0])
    ax.set_ylabel(labels[1])

    ax.tick_params(axis='x', direction='out', top=False)
    ax.tick_params(axis='y', direction='in', right=False)

    #[TODO] config limits
    ax.set_xlim([0, 100])
    ax.set_ylim([-1, 9])

    ax.set_xticks(x_ticks)
    ax.set_yticks(y_ticks)
    ax.grid(color=(.6, .6, .6), linestyle='--', linewidth=.5)

    legend = ax.legend(loc='upper center', bbox_to_anchor=(0.47, -0.2), ncol = 2, columnspacing=0.2, borderaxespad=0., numpoints=1, fontsize=11.5)
    for l in legend.legendHandles:
        l._legmarker.set_markersize('10')

    print('saving at', out_path)
    # plt.show()
    plt.savefig(out_path+'.'+target_format, format=target_format, bbox_inches='tight')
    plt.clf()
    plt.close('all')


def plot():
    # [TODO] config paths
    p_data = './data/'
    p_plot = './plot/'
    if not os.path.exists(p_plot):
        os.makedirs(p_plot)

    title = 'Teste'
    labels = []

    # [TODO] background lines
    x_ticks = np.arange(0, 101, 10)
    y_ticks = np.arange(-1, 10, 1)
    out_path = p_plot

    sample_info = {}
    for sample in samples:
        f_file = p_data + sample + '.csv'
        levels, labels, data = extract_2D_data(f_file)
        style = sample_style[sample]
        sample_info[sample] = [data, style]

    labels = ["Complexity reduction (%)", "BD-Rate (%)"]
    plot_all_lines(title, sample_info, labels, x_ticks, y_ticks, out_path+"plot", target_format='pdf')


plot()
