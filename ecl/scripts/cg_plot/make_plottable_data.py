from sys import argv

# usage: ./script.py results.csv out/path/
results = open(argv[1], 'r+').read().splitlines()
output_path = argv[2]

# Sequence,BDBR,Candidate Number,Candidate Area,Time
for result in results[1:]:
    sequence, bdbr, _, area, time = result.split(',')
    bdbr = float(bdbr) * 100
    complexity_reduction = (1 - float(area)) * 100
    with open(output_path + sequence + ".csv", 'w+') as data:
        print("name,cost,bdbr", file=data)
        print("{},{},{}".format(sequence, complexity_reduction, bdbr), file=data)
