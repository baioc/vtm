from math import log2, floor, inf
from sys import argv
import matplotlib.pyplot as plt
import numpy as np


MAX_CU_DEPTH = 7
MAX_CU_SIZE = 1 << MAX_CU_DEPTH


def bitrate(x, y, imv_shift=0) -> int:
    return golomb(x >> imv_shift) + golomb(y >> imv_shift)


def golomb(val) -> int:
    length = 1
    temp = (((-val) << 1) + 1) if val <= 0 else val << 1

    while temp > MAX_CU_SIZE:
        length += MAX_CU_DEPTH << 1
        temp >>= MAX_CU_DEPTH

    return length + (floor(log2(temp)) << 1)


def plot_surface(size, thresholds):
    xy_max = size // 2
    xy_min = -xy_max
    xs, ys = np.meshgrid(np.arange(xy_min, xy_max), np.arange(xy_min, xy_max))

    z_max, z_min = -inf, inf
    zs = np.zeros((size, size))
    for (i, j), _ in np.ndenumerate(zs):
        bits = bitrate(xs[i][j], ys[i][j])
        zs[i][j] = bits
        z_max = max(z_max, bits)
        z_min = min(z_min, bits)

    origin = 'upper'
    extent = [xy_min, xy_max - 1, xy_min + 1, xy_max]

    plt.imshow(zs, extent=extent, origin=origin, cmap='plasma')
    bar = plt.colorbar(ticks=range(z_min, z_max + 1, 4))
    bar.ax.set_ylabel("R (bits)", rotation='horizontal', verticalalignment='center',
                      horizontalalignment='left') # this parameter makes no sense

    countour_set = plt.contour(zs, thresholds, extent=extent, origin=origin, colors='black')
    # plt.clabel(countour_set, thresholds, fmt="%d") # makes too many labels because contour is open

    plt.xlabel("X")
    plt.xticks(ticks=[*range(xy_min, xy_max, 16)]+[xy_max - 1])

    plt.ylabel("Y", rotation='horizontal', verticalalignment='center')
    plt.yticks(ticks=[xy_min + 1]+[*range(xy_min + 16, xy_max + 1, 16)])

    plt.show()


# usage: python script.py [thresholds...]
if __name__ == "__main__":
    plot_surface(128, sorted([int(threshold) for threshold in argv[1:]]))
